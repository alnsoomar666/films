//
//  APIClient.swift
//  movies
//
//  Created by iOS on 5/13/19.
//  Copyright © 2019 iOS. All rights reserved.
//

import Foundation
import Alamofire

class ApiClient {
    static func information (complition : @escaping (Result<InformationsModel ,Error>) -> Void)  {
        AF.request(APIRouter.information).responseDecodable {  (response : DataResponse<InformationsModel>) in
            complition(response.result)
        }
    }
    static func imageDetail (complition : @escaping (Result<result , Error>) -> Void) {
            AF.request(APIRouter.imageDetailApi).responseDecodable {  (response : DataResponse<result>) in
                complition(response.result)
            }
        }
    static func filmDetail (complition : @escaping (Result<filmDetailsModel , Error>) -> Void) {
            AF.request(APIRouter.filmdetail).responseDecodable {  (response : DataResponse<filmDetailsModel>) in
                complition(response.result)
            }
        }
}

