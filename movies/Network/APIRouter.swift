//
//  APIRouter.swift
//  movies
//
//  Created by iOS on 5/13/19.
//  Copyright © 2019 iOS. All rights reserved.
//

import Foundation
import Alamofire

enum APIRouter : URLRequestConvertible {
    func asURLRequest() throws -> URLRequest {
        let url = "\(ProductionServer.baseURL)\(Paths)"
        let safeUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        var urlRequest = URLRequest(url: URL(string: safeUrl!)!)
        print(urlRequest)
        urlRequest.httpMethod = Methods.rawValue
//        urlRequest.httpMethod = headers
        if Methods.rawValue != "GET"{
            if let parameters = parameters {
                do {
                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                }catch{
                    throw AFError.parameterEncodingFailed(reason:.jsonEncodingFailed(error: error))
                }
            }
        }
        return urlRequest
    }
    
    
    
    
    case information
    case imageDetailApi
    case filmdetail
    
    private var Methods : HTTPMethod {
        switch self {
        case .information:
            return .get
        case .imageDetailApi:
            return .get
        case .filmdetail:
            return .get
       
        }
    }
  
    private var Paths : String {
        switch self {
        case .information:
            
            return "movie/299536/videos?api_key=14f5cc9c009d46efae06b2838783af1e&fbclid=IwAR0PyExT42bqMc3bdjDLwNFRylIbJG8ZrQC48g78VBE4IfYgG2Iywy2HANI"
        
        case .imageDetailApi:
            
            return "movie/popular?api_key=a29c3a5bf40d00b0943c5f7fbfce5b7f&language=en-US&page=1&fbclid=IwAR3oKHSKmZe-NG9IpQ2ZDBqSxwROuCCarafOsCdnTDUUZIsMP86JpqPTeHI"
            
            
        case .filmdetail:
            
            return "299536/reviews?api_key=14f5cc9c009d46efae06b2838783af1e&fbclid=IwAR0nWJRNfVCa51TZam_rRyIRt2_PjBbtVhA6h6DbaRpZ0K-UbPYWxmSmQwY"
       
        }
    }
    private var parameters :Parameters?{
        switch self {
        
        
        case .information:
            return [:]
        case .imageDetailApi:
            return [:]
        case .filmdetail:
            return [:]
        }
    }
       private var headers : HTTPHeaders {
        
            switch self {
                
            case .information:
                return [:]
            case .imageDetailApi:
                return [:]
            case .filmdetail:
                return [:]
            }
        }
}
