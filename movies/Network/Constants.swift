//
//  Constants.swift
//  movies
//
//  Created by iOS on 5/13/19.
//  Copyright © 2019 iOS. All rights reserved.
//

import Foundation
import Alamofire


struct ProductionServer {
    static let baseURL = "https://api.themoviedb.org/3/"
    static let baseImageUrl = "https://image.tmdb.org/t/p/w500/"
}


