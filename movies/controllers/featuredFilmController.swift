//
//  ViewController.swift
//  movies
//
//  Created by iOS on 5/2/19.
//  Copyright © 2019 iOS. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class featuredFilmController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let reuseIdentifier = "cell"
    var featuredFilms : featuredFilmController?
    var imageDetail = [ImageDetailsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = .white
        collectionView.register(featureOfCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        navigationItem.title = "movies"
        setupView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ApiClient.imageDetail { (result) in
            switch result{
            case .success(let image):
                self.imageDetail = image.results
                 self.collectionView?.reloadData()
            case .failure(let failure):
                print("there is an error: ",failure)
                
            }
        }
        
    }
    func setupView() {
        
        
    }
//    func showFilmDetail (film : ImageDetailsModel){
//        let filmDetailController = FilmDetailController(collectionViewLayout: UICollectionViewLayout())
//        navigationController?.pushViewController(filmDetailController, animated: true)
//
//    }

        
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
    return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! featureOfCell
        cell.imagedetail = imageDetail
        cell.appsCollectioView.reloadData()
        return cell
    }    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width,height: 250)
    }
    
}


