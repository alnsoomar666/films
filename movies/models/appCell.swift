//
//  appCell.swift
//  movies
//
//  Created by iOS on 5/14/19.
//  Copyright © 2019 iOS. All rights reserved.
//

import UIKit
import Kingfisher

class appCell : UICollectionViewCell {
    
    var imagedetail : ImageDetailsModel? {
        didSet{
            guard let photoDetail = imagedetail else {return}
            let url = URL(string: ProductionServer.baseImageUrl + photoDetail.posterPath)
            imageview.kf.indicatorType = .activity
            imageview.kf.setImage(with: url)
            
        }
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupsView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let imageview : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 20
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    
    func setupsView (){
        
        addSubview(imageview)
        imageview.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
    }
    
}
