//
//  File.swift
//  movies
//
//  Created by iOS on 5/6/19.
//  Copyright © 2019 iOS. All rights reserved.
//

import UIKit
import Kingfisher


class featureOfCell : UICollectionViewCell, UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    let cellId = "appCellId"
    var imagedetail = [ImageDetailsModel]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    let appsCollectioView : UICollectionView = {
        
        let collectionViewLayout = UICollectionViewFlowLayout()
        let collectioView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionViewLayout.scrollDirection = .horizontal
        collectioView.backgroundColor = .clear
        collectioView.translatesAutoresizingMaskIntoConstraints = false
        return collectioView
    }()
    let headerLabel : UILabel = {
        let label = UILabel()
        label.text = "Best New Films"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.backgroundColor = UIColor.init(red: 149/255, green: 204/255, blue: 244/255, alpha: 1)
        label.textColor = UIColor.white
        label.layer.cornerRadius = 20
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
//    let imageLogo : UIImageView = {
//        let imageView = UIImageView()
//        imageView.contentMode = .scaleToFill
//        imageView.layer.cornerRadius = 20
//        imageView.layer.masksToBounds = true
//        imageView.backgroundColor = UIColor.red
//        return imageView
//    }()
//    let dividerLineView: UIView = {
//        let view = UIView()
//        view.backgroundColor = UIColor(white: 0.4, alpha: 0.4)
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
    
    func setupView() {
        backgroundColor = .clear
        appsCollectioView.register(appCell.self, forCellWithReuseIdentifier: cellId)
        appsCollectioView.delegate = self
        appsCollectioView.dataSource = self
        addSubview(appsCollectioView)
        addSubview(headerLabel)
//        addSubview(imageLogo)
//        addSubview(dividerLineView)
//        imageLogo.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
//        imageLogo.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
//        imageLogo.rightAnchor.constraint(equalTo: headerLabel.leftAnchor, constant: 0).isActive = true
//        imageLogo.widthAnchor.constraint(equalToConstant: 50).isActive = true
        headerLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8 ).isActive = true
        headerLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        headerLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        headerLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        appsCollectioView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 5).isActive = true
        appsCollectioView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 14).isActive = true
        appsCollectioView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0 ).isActive = true
        appsCollectioView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
//        dividerLineView.topAnchor.constraint(equalTo: appsCollectioView.bottomAnchor, constant: 8).isActive = true
//        dividerLineView.leftAnchor.constraint(equalTo: leftAnchor, constant: 14).isActive = true

    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imagedetail.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard  let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? appCell else{return UICollectionViewCell()}
        cell.imagedetail = imagedetail[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 3, bottom: 0, right: 3)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 290, height:frame.height)
    }
    
    }


