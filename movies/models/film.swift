//
//  models.swift
//  movies
//
//  Created by iOS on 5/8/19.
//  Copyright © 2019 iOS. All rights reserved.
//

import UIKit

struct InformationsModel : Codable {
    var id : Int?
    var name : String?
    var site : String?
    var size : Int?
    var type : String?
}

struct ImageDetailsModel :Codable {
    var title : String
    var posterPath : String
    var overview : String
    var  date: String
}
extension ImageDetailsModel{
    enum CodingKeys:String,CodingKey{
        case title,overview
        case date = "release_date"
        case posterPath = "poster_path"
    }
}
struct filmDetailsModel : Codable {
    
   var author : String?
    var content : String?
}
    
struct result : Codable{
    var results : [ImageDetailsModel]
}
