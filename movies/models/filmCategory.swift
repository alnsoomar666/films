//
//  filmCXatgory.swift
//  movies
//
//  Created by iOS on 5/13/19.
//  Copyright © 2019 iOS. All rights reserved.
//

import UIKit

extension InformationsModel {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case site
        case size
        case type
    }
    
    
}

extension ImageDetailsModel {
    enum CodingKeys: String, CodingKey {
        case title
        case posterPath
        case overView
        case date
        
    }
    
    
}
extension filmDetailsModel {
    enum CodingKeys: String, CodingKey {
        case author
        case content
        
    }
    
    
}


